<?php // $Id$
/**
 * @file block-suckerfish.tpl.php
 *
 * Theme implementation to display a block.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 */
?>
<?php print $block->content; ?>